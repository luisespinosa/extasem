# README #

This repository contains automatically generated lexical taxonomies in 6 domains of knowledge, using the **ExTaSem!** pipeline described in [1]. These taxonomies (located in the /resources folder) are provided both in html and csv format. The csv files can be explored with the gephi [2] graph visualization tool. 

------------------------------------

[1] Espinosa-Anke, L., Saggion, H., Ronzano, F., & Navigli, R. (2016, February). Extasem! extending, taxonomizing and semantifying domain terminologies. In Proceedings of the 30th Conference on Artificial Intelligence (AAAI’16).

[2] gephi.org